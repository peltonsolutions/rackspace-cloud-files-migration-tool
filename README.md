# README #

This script allows you to copy Rackspace Cloud Files from one account to another account (and across regions) utilizing swiftly.

This project is provided as-is with no guarantee it will work properly.  It solved the problem I was having.

### How do I get set up? ###

* Install swiftly (http://www.rackspace.com/knowledge_center/article/installing-the-swiftly-cloud-files-client)
* Download this project's php script
* Edit the SOURCE_USER, SOURCE_API_KEY, SOURCE_REGION with the Rackspace Cloud Files account that you want to copy from.
	* To locate your API Key: http://www.rackspace.com/knowledge_center/article/view-and-reset-your-api-key
* Edit the DESTINATION_USER, DESTINATION_API_KEY, DESTINATION_REGION with the Rackspace Cloud Files account that you want to copy to.
* Edit the $containers array with the containers you want to copy.
* Manually create the destination containers
* Run the script.  It will output a count of how many files it's going to copy from each container.

### Who do I talk to? ###

* If you find a bug or have a feature request you can email me at nathan@peltonsolutions.com.