<?php
/**
 * This script allows you to copy Rackspace Cloud Files from one account to another account (and across regions) utilizing swiftly.
 * 
 * @author Nathan Pelton <nathan@peltonsolutions.com>
 * @version 1.0
 */

// Source Rackspace Cloud Files Authentication
$source = array(
	'user'		=> 'SOURCE_USER',
	'api-key'	=> 'SOURCE_API_KEY',
	'region'	=> 'SOURCE_REGION', // Example: IAD
);

// Destination Rackspace Cloud Files Authentication
$destination = array(
	'user'		=> 'DESTINATION_USER',
	'api-key'	=> 'DESTINATION_API_KEY',
	'region'	=> 'DESTINATION_REGION', // Example: DFW
);

// List of containers to copy (Note: destination container must be created manually)
$containers = array(
 	"sample1.containername.com",
	"sample2.containername.com",
	"sample3.containername.com",
	"sample4.containername.com",
);

$source_command_prefix = "swiftly --auth-url=https://identity.api.rackspacecloud.com/v2.0 --auth-user={$source['user']} --auth-key={$source['api-key']} --region={$source['region']}";
$destination_command_prefix = "swiftly --auth-url=https://identity.api.rackspacecloud.com/v2.0 --auth-user={$destination['user']} --auth-key={$destination['api-key']} --region={$destination['region']}";

foreach ( $containers as $container ) {

	// Get the list of files in the source container
	$cmd = "$source_command_prefix get $container";
	$source_files = array_filter(explode("\n", `$cmd`));

	// Get the list of files in the destination container
	$cmd = "$destination_command_prefix get $container";
	$destination_files = array_filter(explode("\n", `$cmd`));

	// Get the list of files not yet copied to the destination
	$files = array_diff($source_files, $destination_files);

	echo "Download ".count($files)." from $container\n";
	
	foreach ( $files as $file ) {
		// Download and upload each file
		$cmd = "$source_command_prefix get '/$container/$file' --sub-command='$destination_command_prefix put \"/$container/$file\"' ";
		echo `$cmd`;
	}
}

// Clear out authentication variables
$source = null;
$destination = null;
